<div class="page-wrapper ssWrap ss-dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>تنظیمات سیستم </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>slug</th>
                                    <th>عنوان</th>
                                    <th>مقدار</th>
                                    <th>دسته بندی</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($options as $option): ?>
                                <tr>
                                    <td><?= $option->option_slug ?></td>
                                    <td><?= $option->option_title ?></td>
                                    <td><?= $option->option_value ?></td>
                                    <td><?= $option->option_cat ?></td>
                                </tr>
                                <?php endforeach;?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
