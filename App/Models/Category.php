<?php
namespace App\Models;

class Category extends BaseModel {
	protected $table = 'categories' ;
	protected $primaryKey = 'id' ;

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'category_attributes', 'category_id', 'attribute_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}