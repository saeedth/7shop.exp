<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class PaymentController{

    public function index($request)
    {
        $data = [];
        View::load('panel.payment.index', $data, 'panel-admin');
    }


}